#!/usr/local/bin/csi -script

(module vectr
 (export <vectr> vectr? <point> point? define-vectr vectr-add get-head get-tail
  set-head! set-tail! vectr-magnitude vectr-length vectr-scalar vectr-subtract
  pointing-up? pointing-down? horizontal? vertical? pointing-left?
  pointing-right?)
 (import scheme (chicken base) (chicken module) coops coops-primitive-objects)

 ; variable naming conventions:
 ; x, y -> x coordinate, y coordinate
 ; v or vn -> vector or vector n
 ; p or pn -> point or point n
 ; a, b, or c -> a^2 + b^2 = c^2

 (define-record-type vectr
  (mk-vectr tail head)
  vectr?
  (head head)
  (tail tail))

 (define-primitive-class <vectr> (<record>) vectr?)

 (define (point? obj)
  (and (pair? obj) (number? (car obj)) (number? (cdr obj))))

 (define-primitive-class <point> (<pair>) point?)

 (define-method (define-vectr (tail-x <number>) (tail-y <number>)
                               (head-x <number>) (head-y <number>)) 
  (mk-vectr (cons tail-x tail-y) (cons head-x head-y)))

 (define-method (define-vectr (tail <pair>) (head <pair>))
  (mk-vectr tail head))

 (define-method (vectr-add (v1 <vectr>)
                           (v2 <vectr>))
  (let ((a (abs (- (car (head v1)) (car (tail v1)))))
        (b (abs (- (cdr (head v1)) (cdr (tail v1))))))
   (define theta (atan (/ b a)))
   (define c (sqrt (+ (expt a 2) (expt b 2))))
   (mk-vectr (cons (+ (car (head v2))
                        (* (cos theta) c)) 
                     (+ (cdr (head v2))
                        (* (sin theta) c)))
               (tail v2))))

 (define-method (get-head (v <vectr>))
  (head v))

 (define-method (set-head! (v <vectr>)
                           (p <point>))
  (set! (head v) p))

 (define-method (get-tail (v <vectr>))
  (tail v))

 (define-method (set-tail! (v <vectr>)
                           (p <point>))
  (set! (tail v) p))

 (define-method (vectr-magnitude (v <vectr>))
  (let ((a (abs (- (car (head v)) (car (tail v)))))
        (b (abs (- (cdr (head v)) (cdr (tail v))))))
   (sqrt (+ (expt a 2) (expt b 2)))))

 ; use this if magnitude is not as clear to those reading your code
 (define-method (vectr-length (v <vectr>))
  (vectr-magnitude v))

 (define-method (vectr-scalar (v <vectr>)
                              (s <number>))
  (let ((a (abs (- (car (head v)) (car (tail v)))))
        (b (abs (- (cdr (head v)) (cdr (tail v)))))
        (new-point (tail v)))
   (define theta (atan (/ b a)))
   (define new-c (* s (sqrt (+ (expt a 2) (expt b 2)))))
   (mk-vectr (cons (+ (car (head v))
                         (* (cos theta) new-c))
                      (+ (cdr (head v))
                         (* (sin theta) new-c)))
                (tail v))))

 (define-method (vectr-subtract (v1 <vectr>)
                                (v2 <vectr>))
  (let ((vx (vectr-scalar v1 -1)))
   (vectr-add vx v2)))

 (define-method (pointing-up? (v <vectr>))
  (> (cdr (head v)) (cdr (tail v))))

 (define-method (pointing-down? (v <vectr>))
  (< (cdr (head v)) (cdr (tail v))))
 
 (define-method (horizontal? (v <vectr>))
  (= (cdr (head v)) (cdr (tail v))))

 (define-method (pointing-right? (v <vectr>))
  (> (car (head v)) (car (tail v))))

 (define-method (pointing-left? (v <vectr>))
  (< (car (head v)) (car (tail v))))
 
 (define-method (vertical? (v <vectr>))
  (= (car (head v)) (car (tail v))))
 )
