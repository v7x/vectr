#!/usr/local/bin/csi -s

(include "vectr")
(import vectr)

(define v1 (define-vectr 0 0 1 1))
(define v2 (define-vectr (/ 1 2) 0.5 2.5 (/ 5 2)))
(print "length v1:")
(print (vectr-length v1))
(print "length v2:")
(print (vectr-length v2))
(print "addition:")
(print (vectr-length (vectr-add v1 v2)))

(define v3 (define-vectr (/ 1 3) 1 (/ 3 2) 8))
(define v4 (define-vectr 0 0 (/ 1 2) (/ 1 2)))

(print "length v3:")
(print (vectr-length v3))
(print "length v4:")
(print (vectr-length v4))

(set! v4 (vectr-scalar v4 3))
(print "scalar:")
(print (vectr-length v4))

(print "subtract:")
(print (vectr-length (vectr-subtract v3 v4)))
